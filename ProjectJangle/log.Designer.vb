﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class log
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.t_stats = New System.Windows.Forms.RichTextBox()
        Me.t_log = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        't_stats
        '
        Me.t_stats.Dock = System.Windows.Forms.DockStyle.Top
        Me.t_stats.Location = New System.Drawing.Point(0, 0)
        Me.t_stats.Name = "t_stats"
        Me.t_stats.ReadOnly = True
        Me.t_stats.Size = New System.Drawing.Size(284, 21)
        Me.t_stats.TabIndex = 0
        Me.t_stats.Text = ""
        '
        't_log
        '
        Me.t_log.Dock = System.Windows.Forms.DockStyle.Fill
        Me.t_log.Location = New System.Drawing.Point(0, 21)
        Me.t_log.Name = "t_log"
        Me.t_log.ReadOnly = True
        Me.t_log.Size = New System.Drawing.Size(284, 240)
        Me.t_log.TabIndex = 1
        Me.t_log.Text = ""
        '
        'log
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.t_log)
        Me.Controls.Add(Me.t_stats)
        Me.Name = "log"
        Me.Text = "Activity Monitor"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents t_stats As RichTextBox
    Friend WithEvents t_log As RichTextBox
End Class
