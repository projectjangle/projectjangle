﻿
Public Class chat
    Public ip As String

    Public igc As Boolean = False
    Private Sub b_send_Click(sender As Object, e As EventArgs) Handles b_send.Click
        If b_ftp.Text = "..." Then
            If Not t_msg.Text = "" Then
                table.wlink = ""
                'constructmsg(t_msg.Text, numud.Value, ip, "")
                sendpersonal(t_msg.Text, ip, numud.Value)
                rtb_chat.AppendText(t_msg.Text & vbNewLine)
                t_msg.Clear()
            Else
                MsgBox("Message cannot be empty")
            End If
        Else
            sendmessage(ip, retport(ip), "[FILE]" & IO.Path.GetFileName(t_msg.Text))
            rtb_chat.AppendText("File Transfer: " & IO.Path.GetFileName(t_msg.Text) & vbNewLine)
            sendfile(t_msg.Text, ip, retport(ip))
            t_msg.Clear()
            b_ftp.PerformClick()
        End If
    End Sub

    Private Sub chat_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If igc = False Then
            If MsgBox("Close Connection?", MsgBoxStyle.OkCancel, "Connection Closing") = MsgBoxResult.Ok Then
                sendmessage(ip, retport(ip), "[CLS]" & konnekt.l_ipv4.Text.Replace("IP :", ""))
                iskonnekted = False
                konnekt.Show()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub t_msg_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_msg.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            b_send.PerformClick()
        End If
    End Sub

    Private Sub rtb_chat_TextChanged(sender As Object, e As EventArgs) Handles rtb_chat.TextChanged
        rtb_chat.SelectionStart = rtb_chat.Text.Length
        rtb_chat.ScrollToCaret()
    End Sub

    Private Sub chat_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        t_msg.Focus()
    End Sub

    Private Sub b_ftp_Click(sender As Object, e As EventArgs) Handles b_ftp.Click
        If b_ftp.Text = "..." Then
            Dim dlg As New OpenFileDialog
            dlg.Title = "Select File"
            If dlg.ShowDialog = DialogResult.OK Then
                t_msg.Text = dlg.FileName
                t_msg.Enabled = False
                b_ftp.Text = "X"
            End If
        ElseIf b_ftp.Text = "X" Then
            t_msg.Clear()
            t_msg.Enabled = True
            b_ftp.Text = "..."
        End If
    End Sub
End Class
