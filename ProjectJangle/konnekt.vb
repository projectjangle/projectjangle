﻿
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Public Class konnekt

    Dim Message As String = ""
    Public ip As String
    Public Listener As New TcpListener(56107)

    Private Sub konnekt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        chkreptester.Show()
        Dim entry = Dns.GetHostEntry(System.Net.Dns.GetHostName())
        For Each address In entry.AddressList
            If address.AddressFamily = AddressFamily.InterNetwork Then
                l_ipv4.Text = address.ToString
                ip = address.ToString
                Exit For
            End If
        Next
        genuid()
        table.Show()
        log.Show()
        contextlibrary.Add("http://www.azlyrics.com/lyrics/flume/takeachance.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/eminem/whenimgone.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/vancejoy/riptide.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/arcticmonkeys/505.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/halsey/colors.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/postalservice/suchgreatheights.html")
        contextlibrary.Add("http://www.azlyrics.com/lyrics/mattkim/daylight.html")

    End Sub
    Sub genuid()
        Dim z As New Random(System.DateTime.Now.Second)
        l_uid.Text = z.Next(1, 26) & z.Next(27, 100)
    End Sub
    Sub listening()
        Listener.Start()
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Listener.Pending = True Then
            Message = ""
            Client = Listener.AcceptTcpClient()
            If rmode = 1 Then
                Dim Reader As New StreamReader(Client.GetStream())
                'This part breaks if user is sending message to self either via direct ip or 127.0.0.1 or localhost
                While Reader.Peek > -1
                    Message = Message + Convert.ToChar(Reader.Read()).ToString
                End While
                procmsg(Message)
                wlog("IN: " & Message)
            ElseIf rmode = 2 Then
                Dim netstream As NetworkStream = Client.GetStream()
                Dim RecData As Byte() = New Byte(BufferSize - 1) {}
                Dim RecBytes As Integer
                Dim totalrecbytes As Integer = 0
                Dim Fs As New FileStream(Application.StartupPath & "/" & savefilename, FileMode.OpenOrCreate, FileAccess.Write)
                While (InlineAssignHelper(RecBytes, netstream.Read(RecData, 0, RecData.Length))) > 0
                    Fs.Write(RecData, 0, RecBytes)
                    totalrecbytes += RecBytes
                End While
                Fs.Close()
                chat.rtb_chat.AppendText("Received File: " & savefilename & vbNewLine)
                savefilename = ""
                rmode = 1
            End If
        End If
    End Sub
    Private Shared Function InlineAssignHelper(Of Listener)(ByRef target As Listener, value As Listener) As Listener
        target = value
        Return value
    End Function
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'sendmessage(dg.SelectedRows.Item(0).Cells(0).Value, dg.SelectedRows.Item(0).Cells(1).Value, "[REQ]" & ip)
        table.wlink = ""
        constructmsg("[REQ]" & ip, -1, dg.SelectedRows.Item(0).Cells(0).Value, "")
    End Sub

    Private Sub t_ip_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Button1.PerformClick()
        End If
    End Sub

    Private Sub konnekt_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Listener.Stop()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If t_ipadd.Text = "" Or t_port.Text = "" Or t_ipadd.Text = "127.0.0.1" Or t_ipadd.Text = l_ipv4.Text Then
            MsgBox("IP Address or Port cannot be blank or equal to self!", MsgBoxStyle.OkOnly, "Error.")
        Else
            sendmessage(t_ipadd.Text, t_port.Text, "[ADDIP]" & ip & ":" & l_ipv4p.Text & ":" & l_uid.Text)
            t_ipadd.Clear()
            If Not cb_port.Checked = True Then t_port.Clear()
        End If
    End Sub

    Private Sub t_ipadd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_ipadd.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            t_port.Focus()
        End If
    End Sub

    Private Sub l_ipv4p_TextChanged(sender As Object, e As EventArgs) Handles l_ipv4p.TextChanged

    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        If Not l_ipv4p.Text = "" Then
            Listener = New TcpListener(l_ipv4p.Text)
            ip = l_ipv4.Text
            l_ipv4.ReadOnly = true
            l_ipv4p.ReadOnly = True
            Dim ListenerThread As New Thread(New ThreadStart(AddressOf listening))
            ListenerThread.Start()
            Timer1.Enabled = True
            Button3.Enabled = False
            Me.Height = 360
            RadioButton1.Enabled = False
            RadioButton2.Enabled = False
            t_ipadd.Focus()
        Else
            MsgBox("Port cannot be blank", MsgBoxStyle.OkOnly, "Error")
        End If
    End Sub
    Private Function GetMyIP() As String
        Using wc As New Net.WebClient
            Return Encoding.ASCII.GetString(wc.DownloadData("http://ivansaccountingtools.sourceforge.net/ip/index.php")).Replace("?", "")
        End Using
    End Function

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            l_ipv4.Text = GetMyIP()
            ip = l_ipv4.Text
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            Dim entry = Dns.GetHostEntry(System.Net.Dns.GetHostName())
            For Each address In entry.AddressList
                If address.AddressFamily = AddressFamily.InterNetwork Then
                    l_ipv4.Text = address.ToString
                    ip = address.ToString
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub l_ipv4p_KeyPress(sender As Object, e As KeyPressEventArgs) Handles l_ipv4p.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Button3.PerformClick()
        End If
    End Sub

    Private Sub t_port_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_port.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Button2.PerformClick()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        For z = 0 To dg.Rows.Count - 1
            For i = 0 To dg.Rows.Count - 1
                If Not dg.Rows(i).Cells(0).Value = dg.Rows(z).Cells(0).Value Then
                    sendmessage(dg.Rows(z).Cells(0).Value, dg.Rows(z).Cells(1).Value, "[ADDIP]" & dg.Rows(i).Cells(0).Value & ":" & dg.Rows(i).Cells(1).Value & ":" & dg.Rows(i).Cells(2).Value)
                End If
            Next
        Next
    End Sub

    Private Sub dg_SelectionChanged(sender As Object, e As EventArgs) Handles dg.SelectionChanged
        t_con.Text = dg.SelectedRows.Item(0).Cells(0).Value
    End Sub
End Class