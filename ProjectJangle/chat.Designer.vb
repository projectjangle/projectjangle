﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class chat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.b_ftp = New System.Windows.Forms.Button()
        Me.numud = New System.Windows.Forms.NumericUpDown()
        Me.b_send = New System.Windows.Forms.Button()
        Me.rtb_chat = New System.Windows.Forms.RichTextBox()
        Me.t_msg = New System.Windows.Forms.RichTextBox()
        Me.Panel1.SuspendLayout()
        CType(Me.numud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.t_msg)
        Me.Panel1.Controls.Add(Me.b_ftp)
        Me.Panel1.Controls.Add(Me.numud)
        Me.Panel1.Controls.Add(Me.b_send)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 227)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(284, 20)
        Me.Panel1.TabIndex = 1
        '
        'b_ftp
        '
        Me.b_ftp.Dock = System.Windows.Forms.DockStyle.Left
        Me.b_ftp.Location = New System.Drawing.Point(0, 0)
        Me.b_ftp.Name = "b_ftp"
        Me.b_ftp.Size = New System.Drawing.Size(28, 20)
        Me.b_ftp.TabIndex = 3
        Me.b_ftp.Text = "..."
        Me.b_ftp.UseVisualStyleBackColor = True
        '
        'numud
        '
        Me.numud.Dock = System.Windows.Forms.DockStyle.Right
        Me.numud.Location = New System.Drawing.Point(176, 0)
        Me.numud.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.numud.Name = "numud"
        Me.numud.ReadOnly = True
        Me.numud.Size = New System.Drawing.Size(33, 20)
        Me.numud.TabIndex = 7
        Me.numud.Value = New Decimal(New Integer() {1, 0, 0, -2147483648})
        '
        'b_send
        '
        Me.b_send.Dock = System.Windows.Forms.DockStyle.Right
        Me.b_send.Location = New System.Drawing.Point(209, 0)
        Me.b_send.Name = "b_send"
        Me.b_send.Size = New System.Drawing.Size(75, 20)
        Me.b_send.TabIndex = 4
        Me.b_send.Text = "Send"
        Me.b_send.UseVisualStyleBackColor = True
        '
        'rtb_chat
        '
        Me.rtb_chat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtb_chat.Location = New System.Drawing.Point(0, 0)
        Me.rtb_chat.Name = "rtb_chat"
        Me.rtb_chat.ReadOnly = True
        Me.rtb_chat.Size = New System.Drawing.Size(284, 227)
        Me.rtb_chat.TabIndex = 2
        Me.rtb_chat.Text = ""
        '
        't_msg
        '
        Me.t_msg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.t_msg.Location = New System.Drawing.Point(28, 0)
        Me.t_msg.Name = "t_msg"
        Me.t_msg.Size = New System.Drawing.Size(148, 20)
        Me.t_msg.TabIndex = 3
        Me.t_msg.Text = ""
        '
        'chat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 247)
        Me.Controls.Add(Me.rtb_chat)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "chat"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        CType(Me.numud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Timer1 As Timer
    Friend WithEvents Panel1 As Panel
    Friend WithEvents b_send As Button
    Friend WithEvents rtb_chat As RichTextBox
    Friend WithEvents numud As NumericUpDown
    Friend WithEvents b_ftp As Button
    Friend WithEvents t_msg As RichTextBox
End Class
