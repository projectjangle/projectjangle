﻿Imports System.Net.Sockets
Imports System.IO
Imports System.Threading

Module mainapp
    Public contextlibrary As New ArrayList
    'variable iskonnekted is probably obsolete but it is being used 93 times somewhere in the code, not worth risking it.
    Public iskonnekted As Boolean = False
    Public bcontext As String
    Public Client As New TcpClient
    Public rmode = 1
    Public savefilename
    Public Const BufferSize As Integer = 1024
    Public Sub wlog(ByVal z As String)
        log.t_log.AppendText(z & vbNewLine)
    End Sub
    Sub procmsg(ByVal z As String)
        If z.Contains("[REQ]") Then
            Dim proc1 As String = z.Replace("[REQ]", "").Replace("IP: ", "")
            If MsgBox("Accept Connection from " & proc1 & "?", MsgBoxStyle.OkCancel, "New Incoming Connection...") = MsgBoxResult.Ok Then
                'sendmessage(proc1, retport(proc1), "[ACPT]" & konnekt.l_ipv4.Text.Replace("IP: ", ""))
                'sendmessage(proc1, retport(proc1), "[ACPT]" & konnekt.ip)
                table.wlink = ""
                bcontext = givecontext()
                constructmsg("[ACPT]" & konnekt.ip & "&" & bcontext, -1, proc1, "")
                chat.Show()
                chat.igc = False
                chat.ip = proc1
                chat.Text = proc1
                konnekt.Hide()
                iskonnekted = True
            End If
        ElseIf z.Contains("[ACPT]") Then
            Dim z1 As String() = z.Split("&")
            Dim proc1 As String = z1(0).Replace("[ACPT]", "").Replace("IP: ", "")
            konnekt.l_stats.Text = "Status: Connection Accepted from " & proc1
            bcontext = z1(1)
            chat.Show()
            chat.igc = False
            chat.ip = proc1
            chat.Text = proc1
            konnekt.Hide()
            iskonnekted = True
        ElseIf z.Contains("[CLS]") Then
            Dim proc1 As String = z.Replace("[CLS]", "").Replace("IP: ", "")
            konnekt.Show()
            MsgBox("Connection Closed by " & proc1, MsgBoxStyle.OkOnly, "Connection closed.")
            konnekt.l_stats.Text = "Status: Pending..."
            chat.igc = True
            chat.Close()
            iskonnekted = False
        ElseIf z.Contains("[CTG]") Then
            generatecontext(z.Replace("[CTG]", ""), 1)
        ElseIf z.Contains("[CTB]") Then
            bcontext = z.Replace("[CTB]", "")
        ElseIf z.Contains("[FILE]") Then
            savefilename = z.Replace("[FILE]", "")
            rmode = 2
        ElseIf z.Contains("[CVB]") Then
            chat.Show()
            generatecontext(bcontext, 1)
            Dim azze As String = z.Replace("[CVB]", "")
            Dim decodedmsg As String
            Dim zer As String() = azze.Split(" ")
            For i = 1 To zer.Count - 1
                decodedmsg = decodedmsg & JangleDecode(zer(i))
            Next
            z = decodedmsg
            chat.rtb_chat.AppendText(z & vbNewLine)
        ElseIf z.Contains("[ADDIP]") Then
            Dim zar As String = z.Replace("[ADDIP]", "")
            Dim zaar As String() = zar.Split(":")
            If Not chkvalue(zaar(0)) = True Then
                konnekt.dg.Rows.Add(New String() {zaar(0), zaar(1), zaar(2)})
                sendmessage(zaar(0), zaar(1), "[ADDIP]" & konnekt.l_ipv4.Text & ":" & konnekt.l_ipv4p.Text & ":" & konnekt.l_uid.Text)
            End If
        ElseIf z.Contains("[PO") Then
            Dim zaa As String() = z.Split("]")
            Dim aza As String() = zaa(0).Split(":")
            'Format should be [PO:3:192.168.0.1:4908]
            If Not CInt(aza(1)) = 1 Then
                constructmsg(zaa(1), aza(1) - 1, aza(2), aza(3))
            Else
                'Should we append the message to this command? E.G. [PO:3:192.168.0.1]Message Text Here
                'If forwarding interval = 1, then next receiver should be end receiver(192.168.0.1).
                If Not table.wlink = "" Then
                    Dim decodedmsg As String
                    Dim zer As String() = zaa(1).Split(" ")
                    For i = 1 To zer.Count - 1
                        decodedmsg = decodedmsg & JangleDecode(zer(i))
                    Next
                    zaa(1) = decodedmsg
                End If
                generatecontext(givecontext, 1)
                Dim encodedmsg As String
                For Each ch As Char In zaa(1)
                    encodedmsg = encodedmsg & " " & JangleEncode(Asc(ch))
                Next
                sendmessage(aza(2), retport(aza(2)), "[CTG]" & table.wlink)
                sendmessage(aza(2), retport(aza(2)), encodedmsg)
            End If
        Else
            chat.Show()
            If Not table.wlink = "" Then
                Dim decodedmsg As String
                Dim zer As String() = z.Split(" ")
                For i = 1 To zer.Count - 1
                    decodedmsg = decodedmsg & JangleDecode(zer(i))
                Next
                z = decodedmsg
                With z
                    If .Contains("[REQ]") Or .Contains("[ACPT]") Or .Contains("[CLS]") Or .Contains("[CTB]") Or .Contains("[CVB]") Then
                        procmsg(z)
                    Else
                        chat.rtb_chat.AppendText(z & vbNewLine)
                    End If
                End With
            Else
                chat.rtb_chat.AppendText(z & vbNewLine)
            End If
        End If
    End Sub
    Function chkvalue(ByVal lookupval As String) As Boolean
        For i = 0 To konnekt.dg.Rows.Count - 1
            If konnekt.dg.Rows(i).Cells(0).Value = lookupval Then
                Return True
                Exit Function
            End If
        Next
        Return False
    End Function
    Function retport(ByVal ip As String) As String
        For i = 0 To konnekt.dg.Rows.Count - 1
            If konnekt.dg.Rows(i).Cells(0).Value = ip Then
                Return konnekt.dg.Rows(i).Cells(1).Value
            End If
        Next
    End Function
    Function retid(ByVal ip As String) As String
        For i = 0 To konnekt.dg.Rows.Count - 1
            If konnekt.dg.Rows(i).Cells(0).Value = ip Then
                Return konnekt.dg.Rows(i).Cells(2).Value
            End If
        Next
    End Function
    Function givecontext() As String
        Dim zn As New Random
        Return contextlibrary(zn.Next(0, contextlibrary.Count - 1))
    End Function
    Sub constructmsg(ByVal msg As String, ByVal rt As Integer, ByVal ip As String, ByVal ids As String)
        If Not table.wlink = "" Then
            Dim decodedmsg As String
            Dim zer As String() = msg.Split(" ")
            For i = 1 To zer.Count - 1
                decodedmsg = decodedmsg & JangleDecode(zer(i))
            Next
            msg = decodedmsg
        End If
        Dim z As New Random()
        Dim zzz As New ArrayList
        For i = 0 To konnekt.dg.Rows.Count - 1
            'only add computers that are not 1)Final Destination 2)Sender 3)Have already sent the message
            If Not konnekt.dg.Rows(i).Cells(0).Value = ip Then
                If Not konnekt.dg.Rows(i).Cells(0).Value = konnekt.ip Then
                    If Not ids.Contains(konnekt.dg.Rows(i).Cells(2).Value) Then
                        zzz.Add(konnekt.dg.Rows(i).Cells(0).Value)
                    End If
                End If
            End If
        Next
        If Not zzz.Count = 0 Then
            'check if there are more than 2 computers in network.
            Dim za As Integer = z.Next(0, zzz.Count - 1)
            'check if maximum amount of rotations should be used(val = -1), not practical if network is large.
            If rt = -1 Then rt = zzz.Count
            'Reconstruct pass on command and appends own id to the pass on command.
            Dim az As String = "[PO:" & rt & ":" & ip & ":" & ids & " " & konnekt.l_uid.Text & "]"
            'clear wlink, unsure if required as wlink is cleared everytime context is generated.
            'table.wlink=""
            generatecontext(givecontext, 1)
            sendmessage(zzz(za), retport(zzz(za)), "[CTG]" & table.wlink)
            Dim encodedmsg As String
            For Each ch As Char In msg
                encodedmsg = encodedmsg & " " & JangleEncode(Asc(ch))
            Next
            sendmessage(zzz(za), retport(zzz(za)), az & encodedmsg)
        Else
            'table.wlink=""
            generatecontext(givecontext, 1)
            Dim encodedmsg As String
            For Each ch As Char In msg
                encodedmsg = encodedmsg & " " & JangleEncode(Asc(ch))
            Next
            sendmessage(ip, retport(ip), "[CTG]" & table.wlink)
            sendmessage(ip, retport(ip), encodedmsg)
        End If
    End Sub
    Sub generatecontext(ByVal tz As String, ByVal md As Integer)
        table.gencontext(tz, md)
    End Sub
    Public Sub sendmessage(ByVal ip As String, ByVal port As Integer, ByVal mesg As String)
        Try
            Client = New TcpClient(ip, port)
            Dim Writer As New StreamWriter(Client.GetStream())
            Writer.Write(mesg)
            wlog("OUT: " & mesg)
            Writer.Flush()
        Catch ex As Exception
            Console.WriteLine(ex)
            Dim Errorresult As String = ex.Message
            MessageBox.Show(Errorresult & vbCrLf & vbCrLf &
                            "Please Review Client Address",
                            "Error Sending Message",
                            MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Function JangleEncode(ByVal z As Integer)
        Dim x As Integer
        Dim y As Integer
        Dim azz As Char() = z.ToString.ToCharArray
        If azz.Count = 3 Then
            x = Val(azz(0) & azz(1))
            y = Val(azz(2))
        Else
            x = Val(azz(0))
            y = Val(azz(1))
        End If
        Dim bi As String = table.dg.Rows(y).Cells(x).Value
        Return bi
    End Function
    Function JangleDecode(ByVal z As String) As Char
        For y = 0 To table.dg.Rows.Count - 1
            For zy = 0 To table.dg.Columns.Count - 1
                If table.dg.Rows(y).Cells(zy).Value = z Then
                    Dim zya As String = zy.ToString & y.ToString
                    Return ChrW(CInt(zya))
                End If
            Next
        Next
    End Function
    Sub sendpersonal(ByVal msg As String, ByVal dest As String, ByVal rot As Integer)
        'generate initial layer context with destination
        table.wlink = ""
        generatecontext(bcontext, 1)
        Dim encodedmsg As String
        For Each ch As Char In msg
            encodedmsg = encodedmsg & " " & JangleEncode(Asc(ch))
        Next
        'send message encrypted with initial layer context
        table.wlink = ""
        constructmsg("[CVB]" & encodedmsg, rot, dest, "")
    End Sub
    Sub sendfile(ByVal filename As String, ByVal ip As String, ByVal port As Integer)
        Dim SendingBuffer As Byte() = Nothing
        Dim client As TcpClient = Nothing
        Dim netstream As NetworkStream = Nothing
        Try
            client = New TcpClient(ip, port)
            netstream = client.GetStream()
            Dim Fs As New FileStream(filename, FileMode.Open, FileAccess.Read)
            Dim NoOfPackets As Integer = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Fs.Length) / Convert.ToDouble(BufferSize)))
            Dim TotalLength As Integer = CInt(Fs.Length), CurrentPacketLength As Integer, counter As Integer = 0
            For i As Integer = 0 To NoOfPackets - 1
                If TotalLength > BufferSize Then
                    CurrentPacketLength = BufferSize
                    TotalLength = TotalLength - CurrentPacketLength
                Else
                    CurrentPacketLength = TotalLength
                End If
                SendingBuffer = New Byte(CurrentPacketLength - 1) {}
                Fs.Read(SendingBuffer, 0, CurrentPacketLength)
                netstream.Write(SendingBuffer, 0, CInt(SendingBuffer.Length))
            Next
            chat.rtb_chat.AppendText("Sent " & Fs.Length.ToString() & " bytes to " & ip & vbNewLine)
            Fs.Close()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            netstream.Close()
            client.Close()
        End Try
    End Sub
End Module
'FLOW CHART
'GENERATE CONTEXT WITH B USING CTG COMMAND
'SEND PO COMMAND TO B
'B GENERATE CONTEXT WITH C USING CTG COMMAND
'SEND PO COMMAND TO C
'MUST IMPLEMENT JANGLE ENCODE BEFORE ABLE TO SEND ANY COMMANDS OVER AS SYMBOLS INTERFERES.
