﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class konnekt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.l_stats = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.l_ipv4 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.t_ipadd = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.t_port = New System.Windows.Forms.TextBox()
        Me.dg = New System.Windows.Forms.DataGridView()
        Me.accs_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.amt_d = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgd_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.l_ipv4p = New System.Windows.Forms.TextBox()
        Me.l_uid = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cb_port = New System.Windows.Forms.CheckBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.t_con = New System.Windows.Forms.TextBox()
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'l_stats
        '
        Me.l_stats.AutoSize = True
        Me.l_stats.Location = New System.Drawing.Point(11, 299)
        Me.l_stats.Name = "l_stats"
        Me.l_stats.Size = New System.Drawing.Size(91, 13)
        Me.l_stats.TabIndex = 3
        Me.l_stats.Text = "Status: Pending..."
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(229, 274)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Connect"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'l_ipv4
        '
        Me.l_ipv4.Location = New System.Drawing.Point(79, 35)
        Me.l_ipv4.Name = "l_ipv4"
        Me.l_ipv4.Size = New System.Drawing.Size(96, 20)
        Me.l_ipv4.TabIndex = 5
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(268, 82)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(36, 20)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Add"
        Me.Button2.UseVisualStyleBackColor = True
        '
        't_ipadd
        '
        Me.t_ipadd.Location = New System.Drawing.Point(37, 82)
        Me.t_ipadd.Name = "t_ipadd"
        Me.t_ipadd.Size = New System.Drawing.Size(116, 20)
        Me.t_ipadd.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "IP Address:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 85)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "IP:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(159, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Port:"
        '
        't_port
        '
        Me.t_port.Location = New System.Drawing.Point(194, 83)
        Me.t_port.Name = "t_port"
        Me.t_port.Size = New System.Drawing.Size(68, 20)
        Me.t_port.TabIndex = 12
        '
        'dg
        '
        Me.dg.AllowUserToAddRows = False
        Me.dg.AllowUserToResizeColumns = False
        Me.dg.AllowUserToResizeRows = False
        Me.dg.BackgroundColor = System.Drawing.Color.White
        Me.dg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.accs_d, Me.amt_d, Me.dgd_id})
        Me.dg.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dg.GridColor = System.Drawing.Color.White
        Me.dg.Location = New System.Drawing.Point(11, 108)
        Me.dg.Name = "dg"
        Me.dg.ReadOnly = True
        Me.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg.Size = New System.Drawing.Size(293, 162)
        Me.dg.TabIndex = 13
        '
        'accs_d
        '
        Me.accs_d.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.accs_d.HeaderText = "IP"
        Me.accs_d.Name = "accs_d"
        Me.accs_d.ReadOnly = True
        '
        'amt_d
        '
        Me.amt_d.HeaderText = "Port"
        Me.amt_d.Name = "amt_d"
        Me.amt_d.ReadOnly = True
        Me.amt_d.Width = 75
        '
        'dgd_id
        '
        Me.dgd_id.HeaderText = "ID"
        Me.dgd_id.Name = "dgd_id"
        Me.dgd_id.ReadOnly = True
        Me.dgd_id.Width = 50
        '
        'l_ipv4p
        '
        Me.l_ipv4p.Location = New System.Drawing.Point(216, 35)
        Me.l_ipv4p.Name = "l_ipv4p"
        Me.l_ipv4p.Size = New System.Drawing.Size(40, 20)
        Me.l_ipv4p.TabIndex = 14
        '
        'l_uid
        '
        Me.l_uid.AutoSize = True
        Me.l_uid.Location = New System.Drawing.Point(213, 300)
        Me.l_uid.Name = "l_uid"
        Me.l_uid.Size = New System.Drawing.Size(55, 13)
        Me.l_uid.TabIndex = 15
        Me.l_uid.Text = "Unique ID"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(262, 36)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(43, 20)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "Start"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(125, 12)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(108, 17)
        Me.RadioButton1.TabIndex = 17
        Me.RadioButton1.Text = "Public IP Address"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Location = New System.Drawing.Point(14, 11)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton2.TabIndex = 18
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Local IP Address"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(181, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Port:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 66)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 13)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Add Contacts:"
        '
        'cb_port
        '
        Me.cb_port.AutoSize = True
        Me.cb_port.Location = New System.Drawing.Point(221, 65)
        Me.cb_port.Name = "cb_port"
        Me.cb_port.Size = New System.Drawing.Size(83, 17)
        Me.cb_port.TabIndex = 21
        Me.cb_port.Text = "Port is same"
        Me.cb_port.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(12, 274)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 22
        Me.Button4.Text = "Share"
        Me.Button4.UseVisualStyleBackColor = True
        '
        't_con
        '
        Me.t_con.Location = New System.Drawing.Point(93, 276)
        Me.t_con.Name = "t_con"
        Me.t_con.Size = New System.Drawing.Size(130, 20)
        Me.t_con.TabIndex = 23
        '
        'konnekt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 65)
        Me.Controls.Add(Me.t_con)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.cb_port)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.l_uid)
        Me.Controls.Add(Me.l_ipv4p)
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.t_port)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.t_ipadd)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.l_ipv4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.l_stats)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "konnekt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Connect"
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents l_stats As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents l_ipv4 As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents t_ipadd As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents t_port As TextBox
    Friend WithEvents dg As DataGridView
    Friend WithEvents accs_d As DataGridViewTextBoxColumn
    Friend WithEvents amt_d As DataGridViewTextBoxColumn
    Friend WithEvents dgd_id As DataGridViewTextBoxColumn
    Friend WithEvents l_ipv4p As TextBox
    Friend WithEvents l_uid As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cb_port As CheckBox
    Friend WithEvents Button4 As Button
    Friend WithEvents t_con As TextBox
End Class
